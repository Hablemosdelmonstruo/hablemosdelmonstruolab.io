---
date: 2019-11-15T19:22:13Z
description: "CD Marathón rechaza propuesta de Motagua de reprogramar La Pentagonal"
featured_image: "/images/vargasamaya.jpeg"
tags: []
title: "Marathón no cede a petición de Motagua de reprogramar las jornadas en La Pentagonal"
---

La polémica se aumento luego que dirigentes de Real España han notificado vía Twitter su apoyo a Motagua para reprogramar el calendario de La Pentagonal.

El cuerpo administrativo y técnico de los Verdolagas no se hicieron esperar y Rolin Peñe mostró el comunicado que se presentara a La Comisión de Disciplina para exponer la ilegalidad de atrasar el campeonato cuando Motagua no presenta motivos legales para la reprogramacion de la fecha.

Peña dijo: "No tenemos por que esperar la respuesta luego de una situación que se aprobó en una asamblea, las bases están aprobadas y solamente una asamblea puede cambiar o modificar este tipo de calendarios y en ese sentido ya pusimos nuestro punto de vista basado en la legalidad y esperamos que se respete".



{{< figure src="/images/comunicadoCD.jpeg" title="Comunicado Oficial de CD Marathon" >}}
