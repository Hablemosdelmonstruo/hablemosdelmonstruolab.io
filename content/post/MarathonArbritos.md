---
title: "Marathon: Comisión Nacional de Disciplina"
date: 2019-11-29T22:25:59Z
description: "“deja como mentirosos” a los árbitros"
featured_image: "/images/Mamon.jpg"
---
Marathón se impone a Comisión Nacional de Disciplina que “deja como mentirosos” a los árbitros

En los últimos torneo, sobre todo en el Apertura 2019, Marathón ha demostrado manejar los tiempos fuera del terreno de las acciones mejor que ningún otro rival, en esta ocasión basados en un vídeo lograron que la Comisión de Disciplina no le impusiera un fuerte castigo al colombiano Yerson Gutiérrez, expulsado frente a Olimpia.

El juez central de dicho encuentro, Melvin Matamoros, auxiliado por uno de sus acompañantes mandó a las duchas al sudamericano y más tarde insertaron en el acta que lo habían expulsado por juego violento por lo que debería pagar al menos dos juegos de suspensión.

{{< figure src="/images/Acta33.jpg" title="Acta del Partido" >}}

No obstante, los miembros de la Comisión Nacional de Disciplina no se pusieron de acuerdo a la hora de castigar al colombiano por lo que solicitaron el vídeo de la acción y al final determinaron que Gutiérrez solo debe ser suspendido un encuentro.

Esta decisión deja abiertos dos escenarios de cara al futuro, el primero que ahora los clubes podrán recurrir cualquier tipo de sanciones basados en un vídeo y el segundo que los árbitros no se apegan a la verdad en sus redacciones arbitrales porque incluso la Comisión Nacional de Disciplina les hace una advertencia al equipo de jueces que estuvo en el duelo entre blancos y verdes.

“Se le hace un llamado a los señores Melvin Matamoros, Rodney Salinas, Melvin Cruz y Selvin Brown miembros de la terna arbitral del juego Olimpia – Marathón celebrado el 10 de diciembre de 2019 en el estadio Nacional de Tegucigalpa para que en el futuro se limiten a consignar en las actas arbitrales los hechos que les consten y que sean veraces a efecto de evitar sanciones erróneas a los participantes de los Juegos de la Liga Nacional”, dice textualmente la resolución emitida por la Comisión de Disciplina.

Los verdes arrancan la pentagonal este sábado en San Pedro Sula frente a Motagua y no podrán contar con el atacante Carlo Costly, sancionado con tres duelos de suspensión por lo que la presencia de Gutiérrez es un punto fundamental.

{{< figure src="/images/Reo.jpg" title="Comiunicado CD Marathon" >}}
