---
title: "Arbritos empañan el clasico"
date: 2019-11-15T18:21:14Z
description: "Molestia por el mal desempeño del central en el clásico. Cuerpo técnico, jugadores y fans opinan lo mismo"
featured_image: "/images/matamoros.png"
tags: []
---
El Profe Vargas se destapa contra la cuarteta arbrital de la jornada 17 al criticar fuertemente el desempeño de los árbitros en el resultado del partido.

El equipo verdolaga mostró su descontento por el trabajo realizado por el árbitro Melvin Matamoros en el clásico nacional. El cuerpo técnico y jugadores salieron inconformes por las decisiones tomadas por Melvin Matamoros que afecto el resultado del partido y complico a la escuadra verdolaga en los partidos restantes de la vuelta y la pentagonal.

Azmahar Ariano se refirió al silbante al decir: "Mi punto de vista es en torno al arbitraje, llevan mucho tiempo haciéndonos daño. A mi me habían hablado del arbitraje, pero yo no lo creía, ya viéndolo me preocupa porque le he cogido cariño a Honduras y me entristece que los árbitros quedan siendo protagonistas en partidos".

Luego el defensa canalero concluyo... "No se si hay campaña en contra del equipo, ellos sabrán y bueno nos toca seguir trabajando y luchando contra lo que sea. Las personas que llevan mucho tiempo aquí saben, saben bien lo que pasa y como se maneja esto."

Ariano termino diciendo y alentando a los verdolagas para que no bajen los brazos y seguir apoyando al club en la pentagonal: "Nosotros vamos ser campeones sea como sea y contra el que sea".

Asi mismo, seguidores del Marathon tambien se han expresado en las redes sociales mostrando su descontento por lo ocurrido.

{{< youtube 0zbjCWlbpeE >}}




