---
title: "Vargas: la petición de Motagua esta fuera de la legalidad"
date: 2019-11-16T01:37:43Z
description: "El Profe Vargas habla de como prepara al equipo para la ultima fecha y el inicio de la Pentagonal"
featured_image: "/images/vargas-habla.jpg"
---
{{< youtube zb4SfKKDTuA >}}

El entrenador de Marathón hablo de como prepara el equipo para la pentagonal, también dice de que piensa de la petición de Motagua, y el tema arbrital de las ultimas jornadas.

El Profe Vargas se refirió a la petición de Motagua de no jugar contra ellos el 23 de noviembre argumentado que debe prepararse para la final de la Liga Concacaf y dijo que es algo fuera de la ley y se atrevió a decir que Fair Play ya no existe en el fútbol.

Acá les dejamos lo que expreso...

“Preparándonos para los partidos frente a Honduras Progreso y Motagua podemos llegar a 40 puntos y romper el récord de 38 que tenía Marathón en vueltas regulares, luego pensaremos en la pentagonal”.

“Agrado quiere agrado, Motagua pudo jugar en Comayagua contra nosotros y nos llevó a Juticalpa, si vos no diste no pidas, me parece que no hay petición que valga, hay que respetar la ley, el Fair Play se terminó en el fútbol cuando Benigno Pineda no cobró la mano de Milton Palacios”.

“Eso de que represento al país y que quiero tiempo, cada uno lleva agua para su molino y el molino nuestro se llama Marathón y buscaremos jugar el día que nos convenga”.

“Lo que están pidiendo no es legal, se aprobó en un congreso, no podemos aceptar la ilegalidad”.

“Aquí no hay desorden, lo que pasa es que no queremos respetar la ley, desde el momento que pones un tal Mourra a manejar los árbitros y trae un extranjero para que los dirija entonces ya es entrar en lo ilegal porque pones un desconocido en un lugar en que luego le vas a pedir un favor algún día, tenemos dos presidentes de Fenafuth presos y gente cercana a ellos sigue manejando la federación”.

“Seguimos cometiendo errores, poniendo al hijo de Salinas cuando Olimpia juega de local y a Matamoros que tuvo un problema con nuestro presidente, nadie se anima a castigarme porque digo verdades”.

“No podemos poner como excusa que no ganamos porque nos cambien el día, pero hay que respetar la ley. En la semifinal contra Motagua no escondieron las pelotas y les pusieron un multa, pero siguen escondiendo las pelotas, la vez pasada lo hizo Olimpia y a lo mejor lo tenemos que hacer nosotros”.

“Hay alguien que quiere sacar a Carlos Costly del partido contra Motagua por eso el castigo de tres partidos”.
