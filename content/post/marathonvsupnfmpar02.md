---
title: "Marathón le da paliza a Lobos"
date: 2021-08-11T18:47:28Z
description: "Marathón muestra su garra ante Lobos en La Cueva"
featured_image: "https://pbs.twimg.com/media/E8i53KyXsAIzgYK?format=jpg&name=medium"
tags: []
---

Marathón le recetó una paliza de 5-1 a Lobos de la UPNFM, esta tarde en el estadio Yankel Rosenthal de San Pedro Sula, en el juego que abrió la segunda jornada del torneo de Apertura de la Liga Nacional.

{{< youtube ZvnfPQbfX4Q >}}

De la mano del técnico uruguayo, Martín “Tato” García, el “monstruo verde” del Marathón lleva dos victorias al hilo que lo ubican en posiciones cimeras. Por su parte Lobos, en las últimas posiciones con únicamente un punto.

Muy parejos los dos equipos en los primeros minutos, fue hasta el 16 que Ovidio Lanza se atrevió a rematar y el balón pasó cerca del marco universitario encomendado a Bryan Ramírez.

Fue hasta los 25 minutos que Marathón encontró el camino al gol  Ovidio Lanza, se encontró un balón suelto en el área estudiosa, remató potente y mandó la pelota al fondo de las redes para el 1-0.

Dos minutos después, en tiro libre  y paralelo al poste izquierdo, Kervin Arriaga, envía el balón sobre la barrera, el esférico se va al ángulo superior izquierdo, vuela el meta Ramírez, pero sólo para adornar el golazo que significó  derecho el 2-0 de los verdolagas.

Desmoronados en defensa los Lobos, seguían cediendo espacios y se les imposibilitaba detener los avances del Marathón, que no conformes insistían con el tercero.

Fue el juvenil Isaac Castillo a los 35 minutos quien con remate a ras de pasto y desde fuera del área pone el 3-0 para Marathón.

La debacle universitaria aumenta cuando  Frelys López desde el mediocampo en velocidad sorprende a la zaga universitaria, llega al área, se quita la marca del arquero Ramírez, y casi sin ángulo logra sacar el remate que termina en la mallas  para el 4-0, paliza con la que acabó la primera parte.

Al inicio de la segunda aparte el recién ingresado Ted Bodden acorta distancias para la UPNFM, una jugada en la que dentro de área, Juan Ramón Mejía la combinó a Bodden quien con tremendo disparo venció a Denovan Torres para el 1-5 a los 47 minutos.

Transcurrían 57 minutos y Mathias Techera, tras un tiro de esquina define fino para poner el quinto y definitivo de Marathón.

### FICHA TÉCNICA:

MARATHÓN (5): Denovan Torres, John Paul Suazo, Mathias Techera, Allan Vargas, Kervin Arriaga (Emilio Izaguirre 63’), Isaac Castillo, Jefry Miranda (Selvin Guevara 46’), Adrián Ramírez, Frelys López (Bryan Castillo 60’), Solani Solano (Jeancarlo Vargas 71’) y Ovidio Lanza (Carlo Costly 60’).

GOLES: O. Lanza 25’, K. Arriaga 27’, I. Castillo 35’, F. López 37’, y M. Techera 57’

AMONESTADOS: Ninguno

UPNFM (1): Bryan Ramírez, Jean Baptiste, Pablo Cacho (Robel Bernárdez 46’),  Lesvin Medina, Aldo Oviedo, Marco Godoy (Ted Bodden 46’)  Axel Gómez, Samuel Elvir, Luis Álvarez (Juan Ramón Mejía 36’), Rembrant Flores y César Guillén.

GOLES: T. Bodden 46’

AMONESTADOS: R. Bernárdez y L. Medina

ÁRBITRO: Melvin Matamoros

ESTADIO: Yankel Rosenthal

