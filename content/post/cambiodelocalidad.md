---
title: "Liga Betcris: Cambio de Sede y Hora"
date: 2021-08-07T01:07:36+01:00
description: "La Liga Betcris se muda al Mitico Francisco Morazan"
featured_image: "https://pbs.twimg.com/media/E75IRMPXEAAatlJ?format=jpg&name=large"
---

#### Cambian sede y hora del partido Honduras Progreso-Marathón

A horas para el arranque del torneo Apertura continúan los cambios de última hora. Primero se suspendió el duelo que daría por inaugurado el torneo entre Real España y Victoria por un bloqueo  de la FIFA al club «jaibo» y ahora la Liga Nacional anunció que el partido entre los equipos Honduras de El Progreso y Marathón a tenido sus modificaciones de fecha y ciudad.

El partido entre Honduras Progreso y Marathón, que estaba programado para el domingo a las 4:00 PM en el estadio Humberto Micheletti, ahora ha pasado al Morazán de San Pedro Sula a las 6:00 de la tarde donde siempre el 'home club' seguirá siendo el cuadro progreseño. 

{{< figure src="https://pbs.twimg.com/media/E8MuXk7WQAIzDNr?format=jpg&name=large" title="Comunicado Oficial" >}}

Fernando Ávila, gerente de los arroceros, explicó la razón de este cambio de último momento que se registró el viernes a altas horas de la noche en una reunión de emergencia que sostuvo la Liga de forma virtual. 'Nuestro patrocinador Betcris hizo un montaje en el estadio Morazán que dura tres días y la Liga Nacional nos solicitó al club de jugar en San Pedro Sula por el problema que tuvo Victoria para jugar ante Real España y cedimos a dicha solicitud porque era imposible trasladar este montaje a La Ceiba o al Micheletti', explicó el dirigente del cuadro progreseño.

Ávila informa que para este encuentro el club ha dispuesto poner a la venta los 3,600 boletos que es el aforo permitido en el Morazán donde las localidades sol sur y este costarán 100 lempiras y el área de silla y preferencia 300, no hay restricciones para que los hinchas verdolagas asistan. 

'Invito a los aficionados a que asistan, habrá un show espectacular con fuego artificiales y música, lo único que pedimos a la gente es que cumpla con las medidas de bioseguridad con su respectiva mascarilla y su bote de alcohol', solicitó el joven gerente progreseño. Para este debut ante los verdolagas, Honduras Progreso hará el estreno oficial de su nuevo entrenador colombiano John Jairo López junto a sus fichajes de la misma nacionalidad Andrés Salazar (portero), Jitson Mosquera (delantero) y Juan Bolaños (defensa), además de refuerzos nacionales como Bayron Méndez, Víctor Arauz, Dixon Ramírez, Patrick Palacios y Rudy Meléndrez.
