---
title: "Nacimiento Verdolaga"
date: 2019-11-29T21:47:28Z
description: "Nacimiento de la Sinfonia Verde"
featured_image: "/images/1979.jpg"
tags: []
---

{{< tweet 1199136365776187392 >}}

El Club Deportivo Marathón celebra hoy su 94 aniversario de vida en medio de tristeza por su derrota inicial en la pentagonal del Apertura 2019-2020 en casa a manos del Motagua.

{{< figure src="/images/2019.jpg" title="CD Marathón | Apertura 2019" >}}

Los “verdes” son el club más antiguo en el fútbol de Honduras, ya que el Olimpia nació en la rama de béisbol en 1912, pero se organizó en el balompié hasta 1926, según la historia del cuadro centenario.



#### NACIMIENTO DE LA “SINFONÍA VERDE”

El cuadro verde surgió en una reunión de amigos en 1925 donde nace la idea de fundar un club de fútbol. Fue Eloy Montes quien recogió la idea y en la noche del 25 de noviembre de 1925, en el establecimiento comercial suyo se improvisó una asamblea preliminar, con varios amigos reunidos como el propio Montes, Chris Sabillón, Kevin Sánchez, Gerardo Fonseca, Rubén Cerrato, Carlos Miranda, Pepe Améndola, Abraham Miselem, entre otros.

En esa reunión se eligió una directiva provisional, haciendo funciones de presidente don Eloy Montes. En la segunda sesión, Eloy había pedido por su cuenta una pelota de fútbol a la casa Montgomery Ward de Chicago (Estados Unidos), pero debido a una confusión idiomática, en lugar de recibir una pelota de fútbol recibieron una de fútbol americano, cuya marca era “Marathón”, de allí la inspiración de su nombre.

Uno de los grandes orgullos del club Marathón es ser el único equipo de Honduras que tiene su estadio, el Yankel Rosenthal, el que se ubica en la colonia La Sabana de San Pedro Sula, contando con un aforo superior a los 10 mil aficionados.

#### LOS TÍTULOS


Los aficionados “verdes” tuvieron que esperar hasta 1979 para ganar su primera de nueve ligas con el técnico nacional Ángel Ramón “Mon” Rodríguez, quien contó en ese tiempo con figuras de la categoría de Roberto “Robot” Bailey, Arturo “Pacharaca” Bonilla, Francisco Javier Toledo, Celso Güity, Efraín “Pucho” Osorio, Ramón “Albañil” Osorio, René “Maravilla” Suazo, Jorge Alberto “Cuca” Bueso, Félix Carranza, Jorge Phoyoú (uruguayo), Alberto Merelles (argentino), Juan Carlos Weber (argentino), entre otros.

{{< figure src="/images/1979.jpg" title="CD Marathón | Primer Titulo 1979" >}}

En 1985, con parte del plantel campeón en 1979, pero dirigido por Gonzalo “Chalito” Zelaya, vencieron al Vida con gol de Roy Padilla Bardales y ganan su segundo título de liga en el duelo decisivo realizado en el estadio General Francisco Morazán.

Marathón en el nuevo siglo es uno de los equipos más ganadores, obteniendo siete títulos locales con entrenadores como Chelato Uclés, el brasileño Flavio Ortega, Nicolás Suazo, el uruguayo Manuel Keosseian (3) y el argentino Héctor Vargas, quien les dio la última Copa en mayo del 2017 venciendo en penales al Motagua. (GG)
