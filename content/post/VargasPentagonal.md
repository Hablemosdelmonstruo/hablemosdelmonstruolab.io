---
title: "Profe Vargas: se pierde La Pentagonal"
date: 2019-11-29T22:37:44Z
description: "Malas noticias para los Verdolagas"
featured_image: "/images/Vargas.jpg"
---
El director técnico del Marathón, Héctor Vargas, fue suspendido cuatro partido por la Comisión de Disciplina, perdiéndose de esa manera el resto de la Pentagonal, tal y como lo adelantó Cronómetro de Diario Tiempo.

Vargas fue expulsado en el primer encuentro entre Marathón y Motagua de la Pentagonal luego de reclamarle airadamente al central, Said Martinez, atizándole con palabras «soeces» según el silbante.
Escrito del acta arbitral Marathón vs. Motagua:

«A Héctor Vargas por emplear lenguaje ofensivo y denigrante: dale un beso a ese hijo de puta; en ese mismo momento ingresó al terreno de juego un utilero de Marathón; no identificado en la lista oficial, agrediendo físicamente al árbitro asistente Walter López, dándole un puñetazo en el estómago».

{{< youtube 4Oiz85L_NR4 >}}

«Finalizado el juego, cuando nos dirigíamos a camerinos fuimos interceptados por una persona desconocida, identificada con la vestimenta de Marathón; que nos insultó y amenazó de muerte e intentó agredirnos cuando dos agentes de policía le dieron persecución y algunos jugadores de Marathón lo detuvieron; minutos después el jugador Mayron Flores, quebró cuatro celosías de vidrio de nuestro camerino de un puñetazo».
Héctor Vargas fuera de la Pentagonal

El argentino se perderá los partidos ante UPNFM, Vida, Olimpia y de quedar lideres de la Pentagonal y clasificar a una final, Vargas no estará presente en el partido de ida.

Carlo Costly: La Comisión de disciplina también anunció que el atacante de los verdes, está habilitado para el encuentro ante UPNFM; tras una apelación se le redujo un partido de los tres que estaba suspendido.

Maynor Flores: El volante Mayron tendrá que pagar una multa de cinco mil lempiras luego de quebrar cuatro celosías del camerino de los árbitros.
