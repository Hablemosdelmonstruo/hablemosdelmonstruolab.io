---
date: 2019-11-15T17:02:58Z
description: "Jugadores Verdolagas convocados al partido de Copa de Naciones: Martinica"
featured_image: "/images/convocados112019.jpeg"
tags: ["selección"]
title: "Convocados Copa de Naciones: Martinica 12/11/2019"
---
Los jugadores verdolagas Edwin Solani, Bryan Barrios y Kervin Arriaga fueron convocados para el encuentro contra la Selección de Martinica.

Una merecida convocatoria para estos jugadores jóvenes que ya han mostrado sus facultades en CD Marathón, y consolidandose en sus funciones dentro del plan táctico del Profe Vargas.

Kevin Arriaga mostró su calidad de contención en la reciente Selección Sub-23 que califico para el Pre-Olímpico que se llevara acabo en Guadalajara, MX. Su gran desempeño en la escuadra nacional convenció a la directiva del Verde para obtener su pase y formar parte de la institución.

Edwin Solani, jugador con gran velocidad y olfato de gol regresa a la convocatoria después de mostrar su potencial por las bandas y buscar el marco contrario creando caos para la defensa contraria. En los pocos minutos que jugo en la ultima convocatoria, Solani mostró que esta para ser parte del plantel que enfrentara las Eliminatorias.

Bryan Barrios ha mostrado en los partidos de liga que es un defensa solido y con mucho contundencia en la zona, solido en la marca y rápido para crear jugadas verticales por las bandas, han impresionado a Coito y permitirle ser llamado a la escuadra nacional.

Les deseamos mucha suerte a los muchachos y sabemos que al ser tomados en cuenta van a desempeñar una buena función en la cancha.
