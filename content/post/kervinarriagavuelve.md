---
title: "Vuelve Kervin Arriaga"
date: 2021-08-11T18:48:28Z
description: "El Regreso de Kervin se ve en la 'Manita'"
featured_image: "https://pbs.twimg.com/media/E8icL7UX0AAEq8p?format=jpg&name=large"
tags: []
---

Kervin Arriaga vuelve a las canchas y lo hace marcando un golazo de tiro libre con Marathón.

{{< tweet 1425591027969187842 >}} 

El mediocampista Kervin Arriaga regres&oacute; a jugar y lo celebr&oacute; anotando un hermoso gol desde el bal&oacute;n parado.

Kervin Arriaga volvió a la vida. El mediocampista, que fue excluido de los Juegos Olímpicos de Tokio 2021 por Miguel Falero por una lesión, este día ha regresado a los terrenos de juego y lo ha hecho marcando un soberbio gol de tiro libre ante la UPN.

Martín "Tato" García pudo contar con él en este juego que el lo barrió en el primer tiempo con un categórico 4-0 y uno de los goles fueron obra del mediocampista, quien cada vez perfecciona aún más su pegada a balón parado.

Al minuto 27' cuando el Marathón ya estaba arriba 1-0 sobre Los Lobos de Raúl Cáveres, consiguió un tiro libre por el costado izquierdo cerca del área, mismo que el volante Kervin Arriaga ejecutó.

El porteño cogió el balón, lo colocó y burlando la defensa con un magistral cobro colocado y potente, venció al joven portero Bryan Cruz, quien se metió con todo y pelota al fondo de las redes. Imposible detenerlo. Era el 2-0 a favor del monstruo.
