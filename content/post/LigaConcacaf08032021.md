---
title: "Liga Concacaf: Partido Preliminar"
date: 2021-08-04T01:07:36+01:00
description: "Hoy empieza La Liga Concacaf en Managua"
featured_image: "/images/ligaconcacaf.jpeg"
---

El CD Marathón tendrá varias bajas para afrontar el partido ante el Cacique Diriangén por la ronda preliminar de la Liga de Concacaf 2021. 

Antes del viaje, el equipo de ‘Tato’ García jugó tres partidos amistosos en la pretemporada. Marathón enfrentó a Parrillas One, Lone FC y Atlético Choloma.

Marathón vuelve a la zona del repechaje tras lograr quedarse con el último boleto del Honduras. En la edición anterior, el club avanzó a los octavos de final luego de vencer al Antigua en la tanda de penales.

En la temporada 2019 enfrentó al Comunicaciones de Guatemala, pero un fatídico gol del rival en los últimos minutos los dejó fuera del torneo.

Esta será la tercera ocasión en la que participará en una nueva temporada de Liga de Concacaf que tiene como defensor campeón a la Liga Deportiva Alajuelense de Alex López.

Marathón recibirá al Cacique Diriangén el próximo martes 10 de agosto. El duelo sería en el Estadio Olímpico Metropolitano de SPS. 

#### Bajas

Por otra parte, pese a la poca cantidad de días de pretemporada, ‘Tato’ manifestó que llegan en óptimas condiciones para jugar el duelo ante el club ‘pinolero’.

No obstante, García enfatizó que tendrán varias bajas para este juego “No creo que viajen todos los refuerzos que llegaron, algunos no están a tope. Mario Martínez y Allans Vargas están trabajando muy bien y estarían dentro de los que viajarían”, arguyó García en un programa de Radio Freedom.

{{< figure src="/images/kevinarriagabaja.jpeg" title="Kervin Arriaga se pierde partido por lesion" >}}

De las bajas que tendrá Marathón ante Diriangén está la del mediocampista Kervin Arriaga, quien presenta molestias en una de sus rodillas. ‘El Misilito’ está prácticamente descartado para el juego del martes.

Otro de los futbolistas que no estará en el duelo del martes es Edwin Solano, quien todavía se encuentra en Estados Unidos aislado por salir positivo de la Covid-19.

El duelo ante Diriangén iniciará a las 8 de la noche el martes en el estadio Nacional de Managua. Posteriormente, el conjunto nicaragüense devolverá la visita el martes 10 de agosto en el estadio Olímpico Metropolitano. 

