---
title: "Costly 3 partidos de suspencion"
date: 2019-11-15T21:12:54Z
description: "Carlos Costly castigo con 3 partidos por falta en El Clásico"
featured_image: "/images/costlysuspendido.jpeg"
tags: []
---
La Comisión de Disciplina determino dar un castigo de tres partidos a Carlos Costly por la fuerte falta contra el "Patón" Mejia durante El Clásico.

Luego de examinar la jugada, La Comisión de Disciplina determino que el uso de fuerza excesiva ameritaba la roja directa que el árbitro mostró al cocherito.

Durante la acción, se puede ver que es una jugada donde ambos jugadores llegan a pelear el balón y chocan al pelear el balón. El Patón Mejia mostró dolor cuando Costly recargo su pie de apoyo.

Con la suspensión de tres partidos, Costly se perdería el ultimo partido de la vuelta y las dos primeras fechas de la Pentagonal. 

La Comisión también revisará la expulsión del colombiano, Yerson Gutiérrez para poder tomar una determinación.
