---
title: "Yustin Arboleda: regresa Costly"
date: 2019-11-29T22:09:15Z
description: "Nacimiento de la Sinfonia Verde"
featured_image: "/images/yustinarboleda.jpg"
tags: []
---
Arboleda: El regreso de Costly puede ser determinante

Yustin Arboleda, referente ofensivo de Marathón, se refirió al encuentro contra Lobos que es determinante para ambas escuadras y dijo que aún se sienten con opciones de ganar la pentagonal, pero que deben imponerse a los estudiosos este sábado en Choluetca.

El colombiano también habló de la vuelta de Carlo Costly, quien no estuvo ante Motagua por sanción y afirmó que el regreso de un referente siempre es importante.

“Todavía podemos llegar a la final, quedan nueve puntos por disputar y nosotros tenemos capacidad para ganarlos, aunque sabemos que no será sencillo”.

“La intensidad que pongamos en Choluteca será clave para vencer a Lobos, los últimos duelos que hemos disputado contra ellos han sido complicados, es un equipo que presiona y tiene un estilo definido”.

“El equipo está muy motivado y con la convicción de poder sacar los tres puntos. El césped artificial cambia la situación, pero no hay que poner excusas, hay que poner muchas ganas y trabajar, por ahí pasa conseguir o no la victoria”.

“El regreso de Carlo Costly nos ayuda mucho, puede ser determinante, es un referente, los equipos ya no solo tienen que preocuparse por marcar a Arboleda, sino a Carlo, Discua, Mario, ojalá que estemos bien en Choluteca”.

“El triunfo de Lobos en La Ceiba puso más interesante la pentagonal, vamos paso a paso, luego veremos si alcanza para estar en la final”.
