---
title: "Remontada Verdolaga en La Primera Jornada"
date: 2021-08-10T22:47:28Z
description: "CD MARATHÓN REMONTA"
featured_image: "https://mls1pelkqwu2.i.optimole.com/Z9KcaDQ-Y8CeV26j/w:552/h:331/q:90/https://www.lnphn.com/wp-content/uploads/2021/08/WhatsApp-Image-2021-08-08-at-21.01.44.jpeg"
tags: []
---

## CD MARATHÓN REMONTA AL HONDURAS PROGRESO EN CIERRE DE JORNADA 1 EN SPS

El CD Marathón venció 2-1 al Honduras de El Progreso en el Estadio Francisco Morazán de San Pedro Sula y que significó el cierre de la jornada 1 del Torneo Apertura 2021 de la Liga BetCris. 

El cuadro ‘verdolaga’ vino desde abajo para remontar el partido ante los dirigidos por Jhon Jairo López, quien se estrenó en el banquillo del equipo ‘ribereño’. 

Honduras Progreso fue mejor equipo en el primer tiempo, prueba de ello es el gol anotado de Juan Bolaños, colombiano que recién firmó con ‘las panteras negras’ para reforzar la zaga defensiva. 

El primer gol del juego llegó al minuto 15′. El balón quedó en el aire luego de un cobro desde la banda izquierda del Marathón, Bolaños se avivó y mandó la pelota al fondo de la red. 

### -Remontada-

En la segunda etapa, ‘el tato’ García, DT del Marathón, metió al campo a Ovidio Lanza, Edwin Solano, Carlo Costly y Bryan Castillo para buscar la remontada. 

En el minuto 58 llegó el premio del empate. Frelys López ejecutó una formidable acción individual que dejó solo y perfilado a Ovidio Lanza, el ex Jocoro definió con el borde interno del botín derecho para vencer la meta rival y colocar el 1-1 momentáneo. 

Tres minutos después llegó el segundo tanto del ‘Monstruo’. Mario Martínez, desde el borde derecho, mandó un centro que se perfilaba para meterse a la portería defendida de Andrés Salazar, el cuidavallas rozó el balón, sin embargo, Edwin Solano estaba justo en la cita para anotar el segundo y definitivo 2-1.

Marathón logró su segundo triunfo consecutivo tras ganar el primer partido de Liga de Concacaf ante el Diriangén de Nicaragua.

-Ficha del juego-

Resultado: Honduras Progreso 1-2 CD Marathón.

Jornada 1. – Estadio Francisco Morazán.

-Alineaciones-

Honduras Progreso: Andrés Salazar, Víctor Arauz, Juan Bolaños, Gregory Gonzáles, Arnaldo Urbina, Edwin Maldonado, Óscar Salas, Geovanny Martínez, Patrick Palacios, Julián Martínez, Christian Zacaza.

CD Marathón: Denovan Torres, Luis Vega, Allans Vargas, Mathías Techera, Adrián Ramírez, Isaac Castillo, Emilio Izaguirre, Mario Martínez, Selvin Guevara, Frelys López, Byron Rodríguez.

-Goles-

HON: Juan Bolaños (15′). CDM: Ovidio Lanza (58′), Edwin Solano (61′).

{{< youtube so_ulUXBHh8 >}}
