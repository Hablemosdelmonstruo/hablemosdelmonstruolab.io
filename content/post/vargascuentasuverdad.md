---
title: "Vargas cuenta su Verdad"
date: 2021-08-10T21:47:28Z
description: "Héctor Vargas cuenta su verdad: La 'traición' que provocó todo..."
featured_image: "https://www.laprensa.hn/csp/mediapool/sites/dt.common.streams.StreamServer.cls?STREAMOID=2GExrBiNisqyTjO9ZfQFVc$daE2N3K4ZzOUsqbU5sYuljgVRWXlxKnexYhcGiJpI6FB40xiOfUoExWL3M40tfzssyZqpeG_J0TFo7ZhRaDiHC9oxmioMlYVJD0A$3RbIiibgT65kY_CSDiCiUzvHvODrHApbd6ry6YGl5GGOZrs-&CONTENTTYPE=image/jpeg"
tags: []
---
##### Articulo Cortesia de Diario Diez

### La 'traición' que provocó todo, la denuncia en la DPI y habla de Orinson Amaya


El ex t&eacute;cnico argentino de Marath&oacute;n, H&eacute;ctor Vargas, charl&oacute; con DIEZ y cont&oacute; detalle a detalle c&oacute;mo se rompi&oacute; su relaci&oacute;n con el presidente, la denuncia que interpuso ante la DPI y su futuro inmediato.

Héctor Vargas se baja de su Frontier doble cabina, se le ve relajado y más tranquilo porque recién completó la dosis de su vacuna contra el Covid-19; claramente ha perdido peso y él lo confirma al asegurar que en estos meses de pandemia bajó más de 20 libras.

Pero específicamente en estas semanas, tras su salida de Marathón, contabiliza 10 menos y lo acredita a su buena alimentación, pero también a una manía que había adoptado en la cuarentena: tomarse una cerveza a diario> por las noches, esto le provocó reflujo, su gastroenterólogo lo puso en tratamiento y acompañado a su dieta, hoy alcanza las 181 libras.

Antes de jalar el gatillo y disparar en esta amplia, reveladora y explosiva entrevista con DIEZ, el 'León de Formosa' no para de rociar sus manos de alcohol con un pequeño bote que no suelta y entre risas acepta que hoy lo hace sin darse cuenta.

Luego se acomoda, se coloca bien el micrófono y fiel a su estilo no dejó nada al azar, contó su verdad sobre su polémica salida de Marathón donde asegura que al día de hoy, le deben tres meses de salario y que se siente entrenador del club porque tiene un contrato vigente.

También, lejos de arremeter contra el presidente Orinson Amaya, lo ha elogiado más allá de las duras declaraciones que brindó hace un mes involucrando a su esposa, su hijo y su casa, pero lo califica como un 'tipo brillante y capaz" que puso el pecho al club en el momento que nadie quería tomarlo.

### LA ENTREVISTA COMPLETA

¿Cómo han sido estas semanas sin la presión de una planificación de trabajo?
Me merecía, aunque sea obligado, un descanso. Fueron nueve años y medio trabajando de forma continua, intentando lograr cosas así sean campeonatos o salvar descensos, pero me merecía estar con mi familia, con mis hijos, tengo un niño de 7 años, así que lo disfruto además de cuidarnos de este virus que no se ha ido.

¿Siente raro levantarse y no tener trabajo?
Al contrario, esa paz que tengo de despertar a mis hijos para sus clases por Zoom a las 8 de la mañana me pone contento, veo a mis tres hijos estudiar, porque se levantan todos juntos, y el que tiene clases presenciales lo llevo a la escuela, lo disfruto.

¿Usted está con ellos ayudándoles en las clases virtuales?
No, mi esposa es licenciada en ciencias sociales, tiene una maestría y es la encargada de la educación, yo soy el que pago la escuela y ella realmente los prepara y lo hace bien porque mi hija, la mayor, estudia medicina, lleva buenas notas, mi otros dos hijos también y ese es mérito de ella.

¿Es verdad que a su hija en la Universidad sus compañeras le preguntan si usted le pega por el carácter que muestra en los medios?
Justamente hace una semana estuve en la casa de Alvin Lone (dueño del equipo Lone FC) y su esposa me decía que veía una imagen mía que me peleaba con todos, que reniego de todo, pero en mi casa nunca he golpeado a mis hijos o a mi esposa, hasta manejando soy tranquilo, aunque se me cruce un carro; una vez Rolin Peña me remarcó eso porque íbamos para Tegucigalpa y un auto se frenó de un solo, lo esquivé y pasé como si nada, no me altero por nada, tengo la tranquilidad necesaria para vivir en paz, cuando me pongo la camisa de un equipo ahí sí me peleo con el que me cruza.
En medio de la entrevista, el DT aceptó su manía por el alcohol líquido, se pasa rociando sus manos cuando está fuera de casa como una medida para evitar contagiarse del Covid. 

¿Cuál es su rutina ahora sin fútbol?
Me despierto a las 6:30 de la mañana a preparar mate, miro un poco de redes sociales que antes no veía mucho porque no tenía tiempo de hacerlo, luego levanto a los hijos, hago vueltas con trámites de pagos, cosas que no hacía y que ahora tengo tiempo.

¿Qué redes sociales utiliza?
Facebook; mi esposa me abrió una cuenta y mi hijo hace un tiempo otra donde tengo a mis compañeros de pueblo, después en WhatsApp tengo un grupo de los que jugamos juntos allá por 1976 hasta 1981 y charlo mucho con ellos porque crecimos desde la infancia.

¿Ayuda en el aseo de la casa?
Ayudo bastante, ahora mi esposa me cuestiona y me dice que me he hecho hondureño, ayudo cada vez menos ja, ja. Me gusta cocinar cuando tengo tiempo, yo he vivido mucho tiempo solo en San Pedro Sula, Puerto Cortés y La Ceiba y me preparaba la comida, ahora en compañía de la familia voy menos a la cocina.

¿Cuánto tiempo al dedica al fútbol actualmente?
Lo más que puedo, me he metido con los Juegos Olímpicos, mi hijo Erick (en Argentina) me ayuda mucho enviándome información de lo que puedo ver, lo de la farándula deportiva no me gusta, pero sí lo que me pueda ayudar como una opción de juego para ir adquiriendo conocimiento, tengo más de 700 partidos en un disco duro, estudio los rivales y el secreto de mi éxito en los últimos años es estar informado en ese aspecto.

¿Cuál es su pasatiempo actualmente?
Estar con mi familia. Disfruto de mi hijo, salgo a caminar con el niño de 7 años, es con el que más paso tiempo, además de disfrutar a mi esposa ya que he estado mucho tiempo separado por estar dos semanas aquí y un día en Tegucigalpa y volver al día siguiente, ahora disfruto de algo que no hacía.

SOBRE SU PASADO RECIENTE
¿Qué enseñanza le dejaron cuatro años en Marathón?
Son muchas como el hecho de que cuando estás unido podés lograr cosas, que cuando juntas un cuerpo técnico se pueden alcanzar objetivos y que conocés gente que a veces te defrauda, no pensé que pasaría de esa manera, son todas enseñanzas, pero me lo habían dicho puntualmente y por no dejar sin trabajo a alguien no corté la situación, ahora me toca a mí estar afuera y la persona, que creo fue el artífice de todo esto, está adentro.

¿En qué cree que se equivocó en su gestión?
Me equivoco en muchas situaciones, sin ninguna duda. En lo que no me equivoqué nunca es que intenté que las cosas funcionaran, ahí están los números, clasificamos a cuatro ediciones consecutivas de Concacaf, ser campeones después de nueve años, ganar una Súpercopa, un récord de puntos cuando el Marathón de Keosseián era muy fuerte, de mucha inversión y lo superamos; salimos campeones de reservas con menores de 20 años.
Este era un equipo que cuando llegué solo aspiraba al sexto lugar, le cambié la mentalidad con la construcción de un gimnasio, un consultorio, sinceramente no tengo más que palabras de agradecimiento con la gente que ha apoyado y debo agradecerle eso al presidente porque todas las cosas que ha logrado las hizo conmigo. Con Keosseián obtuvo una Copa Presidente, pero el resto de los logros lo hicimos juntos.

Cada torneo tenía un presupuesto más bajo y se le iban piezas importantes, aún así competía, ahora quedó la sensación que usted se fue por la puerta de atrás ¿lo siente así?
Eso fue por el tema del chisme que después se comprobó con una declaración jurada de 'Judas' Pineda, ahí me sorprendió porque me lo habían dicho antes que había que sacarlo y no lo hice, ahora se quedó y veremos hasta cuándo resiste el nuevo técnico. Pero ese fue mi error, me equivoqué, no lo hice por no dejar sin trabajo a nadie, pero lo iba a hacer en este torneo, pero llenó la cabeza al presidente y al margen de todo lo que dijo, quizá lo expresó en un momento de cólera, de resentimiento, pero sigo creyendo que es un tipo brillante, un tipo capaz y que le puso el pecho a Marathón cuando era difícil de sostenerlo.

¿Qué pasó con Jorge Pineda? ¿Cómo quedó su relación por lo que menciona de 'Judas' y de esa declaración jurada ante el TNAF?
Nunca fui amigo de él, yo tengo una costumbre y puede dar fe Luis Ayala, Maní Suazo y la gente de utilería y también en otros clubes que invitaba a todos a tomar café para compartir y hablar sobre el entrenamiento una 'horita' más, pero nunca me aceptó (el café) en cuatro años, nunca se integró, no quiso ser partícipe de un cuerpo técnico, no sé si por tomar mi lugar, pero nunca duró tanto tiempo en un club y conmigo sí lo hizo, nunca fue campeón y conmigo sí, esa fue la traición que provocó todo y que se confirmó con la firma que hizo 'Judas' Pineda y me declaró ante el TNAF con un comentario que le dije, pero quién no se enoja y dice 'ojalá te pase esto', es algo que lo comenté en confianza y que en definitiva no estaba mal y era que si me sacaban mal, iba a proceder porque me parecía algo justo, pero no estoy resentido con nadie que no haya firmado una declaración jurada en contra mía, para el resto mi agradecimiento con Orinson y la gente que ha estado cerca, tengo muy buenos amigos en Marathón, los sigo teniendo, sigo hablando con ellos, pero mi error fue no sacar a alguien que hizo daño a la relación y que por eso terminó como terminó.

Pero usted lo llevó a él.
Yo lo acepto, yo venía de Olimpia y Rolin Peña no lo quería porque Yankel lo había sacado en su etapa, entonces estaba entre el zurdo (Daniel) Viera y Jorge Pineda, ahora aprendí algo de Diego Vázquez; una vez me senté cuando él iba a tomar Motagua y yo estaba en Victoria, estaba Yanuario Paz y un amigo Víctor Sánchez, le pregunté por qué no había llevado a (Luis) Giribaldi (PF peruano) y me dijo que le preguntó a Giribaldi que si el día de mañana él se iba, él también lo haría, entonces le dijo que no, que era amigo de la familia Atala y que se quedaría y Diego se inclinó por Patricio Negreira, entonces me quedó esa enseñanza, no estaba equivocado, es lealtad.
Entonces cuando me dijo de Viera y Pineda, yo lo vi como jugador, pero nunca había charlado con Jorge. Yo lo llamo, le pregunté si vendría a trabajar como asistente, le puse esa condición de que si me iba, él también, me dijo 'hay que tener lealtad profe', no lo cumplió, pero sí lo dijo.

Esta es la declaración jurada presentada por Marathón ante el Tribunal de Arbitraje donde Jorge Pineda detalla lo que Héctor Vargas supuestamente decía 'constantemente' en los entrenamientos.
{{< figure src="https://www.diez.hn/csp/mediapool/sites/dt.common.streams.StreamServer.cls?STREAMOID=g0_tx3dtsW2fk0Yl9HJpXs$daE2N3K4ZzOUsqbU5sYurL48Z8Y6DPL$45O26Oz796FB40xiOfUoExWL3M40tfzssyZqpeG_J0TFo7ZhRaDiHC9oxmioMlYVJD0A$3RbIiibgT65kY_CSDiCiUzvHvODrHApbd6ry6YGl5GGOZrs-&CONTENTTYPE=image/jpeg" title="Declaración Jurada" >}}

¿En qué momento le dijeron que debía sacarlo?
Cuando terminó el torneo que fuimos subcampeones y que perdimos ante Olimpia en el Yankel, me comentaron algo, no creí, pensé que eran chismes, fueron dos o tres personas quienes me informaron sobre algunos comentarios, ahí debí tomar la decisión, porque lo que pasó ahora fue 'chismerío'.
Tres semanas antes (de finalizar el Clausura 2021) el presidente me dijo que quería ser campeón, le reconocí mis errores, me dijo que haría un esfuerzo económico para este torneo y tal es el caso que todos los refuerzos que hablamos hoy están en Marathón, hay dos que no se concretaron y que teníamos pláticas avanzadas que eran Ilce Barahona y Carlos Bernárdez, el resto sí vino.

#### Héctor Vargas cree que Jorge Pineda lo traicionó y dar una declaración jurada con palabras que le dijo en un entrenamiento y que no creyó se las contaría al presidente Amaya.

Después de todo lo que ha sucedido ¿cómo cree que queda su imagen?
Igual, no es tanto lo que me ven, sino cómo me vea yo, llevar a Victoria a una final, luego a Concacaf y dos años después desciende, llegar a Olimpia y sacar a todos los jóvenes haciendo debutar a Menjívar pasando por Patón, Chirinos, Pinto, Benguché, ni hablar de Casildo y otros muchachos que hoy los veo en torneos internacionales y en selección, luego a Marathón, así que me veo mejor que nunca.

¿Qué siente que fue lo mejor que hizo en Marathón?>
Cambiarle el ADN porque tenía uno de un equipo que estaba dejando de ser grande, de hecho en Tegucigalpa hicieron dos programas deportivos diciendo que Marathón ya no era grande, que no peleaba campeonatos, no clasificaba a Concacaf y no sé si era porque yo había llegado, pero conmigo volvió a títulos, estar en Concacaf y creo que el presidente es el único en el club que ganó tres veces las vueltas conmigo, realmente fueron muchos logros y la otra es con Real España, en el Yankel yo perdí una sola vez y no dirigí por estar expulsado, después les ganamos en el Olímpico y Morazán con un presupuesto muy inferior a varios equipos.

¿Cuál fue ese punto de quiebre y donde todo cambió entre usted y Marathón?
Antes que terminara el torneo el presidente me invitó a almorzar y lo hablamos, por eso me extraña lo que pasó en el camino ya que le había dicho que quería cortar a Pineda, pues tenía tres años becado y no generaba lo que quería, entonces mi error fue anticiparme (decir que lo quería cortar), tuve que haberlo dicho cuando estuviera todo definido y no que vinieran ese montón de chismes. De hecho en una reunión Rolin Peña se lo dijo de frente a Orinson, 'a usted presidente le han llenado la cabeza de chismes', yo no esperaba esa situación

¿Se terminó esa amistad con Orinson Amaya, ha tenido contactos con él?
Relación ya no tengo, ahora es una relación de procedimientos judiciales porque yo no me fui de Marathón, me dijeron que me fuera, no demandé, ellos me demandaron y por algo que es ilógico porque dí un comentario sin dar nombres.
Después hay procedimientos y denuncias en la DPI (Dirección Policial de Investigaciones), ellos (Marathón) tienen una en San Pedro Sula con declaraciones de los guardias de mi colonia que quisieron sobornarlos con la identificación de esa persona (que intentó sobornarlos) para que lo dejaran pasar y dejar un documento en mi casa que correspondía al 20 de junio y lo dejaron el 9 de julio tirado al lado del auto de mi esposa; lo levanté en una acta notarial y ellos se equivocaron porque esa empresa (de seguridad) es organizada por el patronato de la colonia y me sugieren en ese momento hacer la denuncia porque estaban interrogando a sus empleados, entonces fuimos a la DPI, pidieron el documento original, se hizo una interrogación a Orinson Amaya en DPI y a su abogado porque ese documento fue presentado en la audiencia del 16 de julio en el TNAF, no ha sido validado porque ellos (TNAF) quedaron en averiguar en la DPI de San Pedro Sula ya que ahí dice que me están despidiendo y hay que firmar como recibido como se hace en todos lados. Tengo los datos comprobados donde no recibí nada, solo tiraron un papel donde dice que lo recibí el 20 de junio con una falsificación (de firma) porque eso queda documentado en la colonia, cada cosa que recibimos los residentes queda en un libro que no se puede alterar.

¿Le sorprendió como se expresó Orinson Amaya de usted?
La sorpresa está de escucharlo, pero sigo teniendo la admiración por él, es una persona que pone dinero para el fútbol, se lo dije a él y puedo decirlo veinte mil veces más, no hablé mal de él, sí comenté en una ocasión a Jorge Pineda algo si me pasaba, pero luego lo puso en una declaración jurada, después dije que si hay directivos que no pueden sostenerse por el tema de pandemia que den un paso al costado, no dí nombres, yo nunca dije 'puse mi tarjeta de crédito', eso lo afirmó el periodista y lo toman como que yo lo afirmé, luego dije que algunos (directivos) que se acercan para levantar la copa y luego no están todo el campeonato, pero en ningún momento hablé y te repito, no me fui, me sacaron, no demandé, me demandaron y tuve que defenderme, ahora defiendo mi contrato de forma legal y leal, en cambio la otra parte tuvo procedimientos llamativos donde no dejaban ver el expediente a mi abogado y eso está en el reclamo.

Héctor Vargas asegura que su relación con Orinson Amaya quedó rota, pero lo respeta por el trabajo que ha hecho en Marathón en un momento donde nadie quería tomar al club.

¿Qué valoraciones tomó para no aceptar los 400 mil lempiras que Amaya dijo le ofreció para rescindir el contrato?
En ningún momento me ofrecieron dinero, tengo que aclararlo. Es mentira, podemos hablarlo de frente, él no me ofreció nunca, solo lo agregó para decir que me ofreció algo y que a la gente le llamara la atención porque incluso le ofrecí pagarme por mes porque adquirí compromisos por ese contrato, agarré una casa y de buenas a primeras finiquitar eso y quedar con el compromiso como si no pasara nada no podía y yo se lo aclaré.

¿Existió un financiamiento y descuento de un millón de lempiras por su casa como menciona el presidente?
¿Cuál descuento del millón? El tema lo manejó él, yo no sabía lo del arreglo, yo tengo el compromiso de pago porque la sigo pagando al día pese a los meses que me deben porque hasta el día de hoy me deben tres meses de sueldo porque no he rescindido contrato. Legalmente sigo siendo entrenador de Marathón.

¿Ha habido algún intento legal por querer quitarle su casa?
No, para nada. Estoy al día, el 3 de este mes pagué la cuota como lo he venido haciendo pese a la pandemia.

¿Le dolió que su esposa haya salido salpicada en este problema?
Yo te explico; cuando alguien te promete, porque él me hizo una promesa que sería el entrenador con más campeonatos de Marathón y que lo hablamos tres semanas antes, y que de primas a primeras cambie su manera de pensar, lo primero que debés hacer es buscar un testigo como hacía Bilardo; entonces la segunda vez que voy a hablar con él, aunque dice que no voy a su oficina y sí he ido, tres veces llegué, entonces cuando fui le dije a mi esposa 'si el hombre cambia, grabá para tener constancia de que sí lo dijo y entonces por primera vez desde 1992 que me gradué como entrenador, mi esposa conoció la oficina de un lugar de un club y la llevé para que grabara las cosas porque no podía llevar a nadie de testigo, a Ayala no le podía decir porque es una persona leal, mis hijos son menores de edad y debía ir alguien que me grabara la situación porque es algo mío, no es legal y no puedo presentarlo y nunca lo haré ante el TNAF, pero sí grabé porque era una manera de saber por si el día de mañana dice que no lo dijo.

Dijo Orinson que usted maltrataba a los futbolistas, le pregunto directamente ¿usted maltrata a los jugadores?
Pregúntenles a ellos. Emilio Izaguirre lo dijo hace poco (en una entrevista con DIEZ), él llegó con su hijo el primer día, pensé que estaba su esposa, le pregunté si había llegado con ella, me dijo que no, solo con su hijo, ¿por qué no baja?, le preguntó, 'cuando estaba en Motagua él se quedaba en el carro porque no dejaban', me responde, le dije que no, que lo bajara y le dije a Carlos Flores (gerente de campo) que le trajeran ropa para entrenar y lo ponía a jugar sus 5-10 minutos en los colectivos para que se sintiera bien.
Además ustedes en DIEZ hace unos años me sacaron un reportaje cuando Chirinos quería venir a Marathón y decía 'Vargas, el futbolista al que todos llaman papá', pueden buscarlo y eso que dijo (Orinson) fue por resentimiento y los chismes que le contaron, pero si exigir a un futbolista es tratarlo mal, entonces sí, porque exijo para que vivan de esto.

Vargas asegura que no maltrata futbolistas y pide que les consulten a ellos, pero acepta que sí les exige para su propio bien. 

Amaya llamó paquetes a los fichajes que usted trajo con Palermo y Hoyos ¿habló con Martín Palermo sobre lo que sucedió?
Si tan paquete era Palermo, que si bien es cierto no tenía la calidad de los jugadores que Marathón está acostumbrado a traer como Pando Ramírez, el tico Scott, Brice, Obelar, que eran futbolistas de calidad y que ganaban muy bien, pero él vino por una cifra ínfima, pero el ego mío debería subir porque con Palermo llegué a ganarle una final de grupos a Olimpia y con esos paquetes que él menciona fuimos subcampeones. Lo de Hoyos fue una apuesta por el poco tiempo que había y era en este momento que debíamos trabajarlo con más tiempo, pero en definitiva sigo pensando lo mismo porque con otros extranjeros que vinieron y con Arboleda llegaban a sextos lugares y conmigo Arboleda fue campeón, así que hay mucho mérito.

¿Es usted un técnico caprichoso y que no acepta consejos?
Tengo que ver de quién viene el consejo, el otro día me encontré a un señor en un café, me saluda y me dice 'usted tiene que jugar 4-3-3' y le respondo ''¿en qué momento del partido va? ¿y si voy ganando 1-0?' 'ahí lo manda más atrás', me dice y le preguntó ¿de qué trabaja usted? 'soy famacéutico', entonces siga con la farmacia, tiene mucho conocimiento de eso, yo me paso mucho tiempo estudiando, leyendo, viendo y ahora tengo un poco de autoridad para opinar, pero si me decís de poner un café y qué producto llevar, yo te consulto y lo hice en Olimpia con Juan Carlos Espinoza, veinte mil veces lo hice, también con Nerlin Membreño, pero si viene alguien y le pega a la pelota y se cae y me sugiere algo, me cuesta, soy sincero, porque por lo menos da el ejemplo de cómo se le pega a una pelota.

¿El presidente le recomendó futbolistas, por ejemplo Frelys López que lo mencionó?
Lo puse en la final que perdimos todo el segundo tiempo y no pateó al arco, lo puse contra Platense en el Puerto Cortés titular, jugó 60 minutos y no pateó al arco, perdimos 5-0 y no sé si Frelys fue campeón con Honduras Progreso o goleador y no sé por qué los jugadores que vienen de este equipo no rindieron como Elroy Smith, Frelys López, Ñangui Cardona, Yerson Gutiérrez y vienen firmados por dos o tres años, pero no es mi culpa que no rindan porque Ñangui vino y estuvo preso sin que ustedes supieran y tuve que sacarlo.

¿Cuál es su legado y cómo quieren que lo recuerde la afición?
No pienso en legados ni en cómo quiero que me recuerden, vivo del presente, tengo un hijo chiquito y uno de 40 años, pienso que hay que vivir la vida en el día a día, no lo veo como un legado, luego los números dirán si hice un buen trabajo.
Ya hablo del gimnasio, la clínica y que usó tarjetas para ayudar ¿Qué otras cosas hizo por Marathón que aún no conocemos?>
El grupo GAMA hizo muchas cosas, ellos me ayudaron en el vestuario, cuando llovía se metía el agua, la alfombra no estaba en condiciones y ahora tienen un camerino con pizarra de fútbol y no de escuela, ayudé las veces que pude con mi tarjeta, mi carro donde siempre iba con la utilería, lo hice en todos lados, incluso en Olimpia.

¿Su carácter lo ha traicionado al ser tan frontal?
Es que es la manera de ser para los que queremos cambiar algo, no voy a cambiar porque alguien me paga o me da algo, no hablaré bien de una persona por sacarle algo, ni siquiera con don Rafael Ferrari con quien comencé a tener un buen salario y no abusé de esa posibilidad pidiéndole dinero aún y cuando él me dio un aumento considerable, pero nació de él.

¿Siente que lo extrañarán tras casi 400 partidos?
No sé si me van a extrañar o van a descansar, pero estoy disfrutando alejado.

¿Cómo cree que le irá a su sucesor Tato García?
Le irá bien porque tiene una base de un equipo que se armó y los refuerzos fueron los que sugerí, tiene un techo alto, viene de cuatro clasificaciones consecutivas en Concacaf, de pelear dos campeonatos, si bien el anterior lo hicimos mal, pero este equipo ahora está acostumbrado a pelear los títulos.

DIEZ PREGUNTAS RÁPIDAS A VARGAS
¿Su mayor logro profesional?
Los títulos que me han tocado ganar.

¿Su mayor logro personal?
La familia que tengo. Mis tres hijos con mi esposa hondureña y mis tres hijos con mi primera esposa argentina. Son los dos aciertos más grandes de mi vida.

¿Algún fracaso?
Haber querido ser un jugador técnicamente dotado y no lo logré.

¿Algún sueño por cumplir?
Reunir a mis seis hijos en una mesa y comer con ellos.

¿El mejor futbolista que dirigió?
Choco Lozano por su condición técnica y por liderazgo Carlo Costly.

¿El mejor momento de su carrera?
Este que actualmente tengo.

¿El equipo que usted desearía dirigir?
A Estudiantes de La Plata.

¿A qué equipo no dirIgiría?
El ofrecimiento al trabajo hay que agradecerlo y estoy con la camiseta de mi familia.

¿Dónde quiere acabar su carrera?
En Platense por ser un lugar donde me ofrecieron trabajo después de estar por años sin hacerlo.

¿El peor insulto recibido?
>Un se&#
