---
title: "Resultados del Partido"
date: 2021-08-04T01:12:36+01:00
description: "Resultado del partido Diriangén vs Marathón, Liga Concacaf 2021"
featured_image: "https://pbs.twimg.com/media/E761b0jXMAIN-GV?format=jpg&name=large"
tag: []
---

Se llevó a cabo el partido de Diriangén vs. Marathón correspondiente al encuentro de ida de la ronda preliminar de la Liga Concacaf 2021. ¡A continuación te contamos cuál fue el resultado!

Así quedó el partido Diriangén vs. Marathón de la ronda preliminar
El conjunto sampedrano salió victorioso del Estadio Nacional de Managua, frente a los nicaragüenses, por marcador de 1-0, siendo el delantero hondureño, Mario Martínez el autor del único tanto del juego, y que a la postre, se llevaría el premio al mejor jugador del partido.

Un golazo de tiro libre por parte del «Lobo» apenas comenzando la segunda parte, al minuto 48′, causó la euforia entre los jugadores y del entrenador del cuadro sampedrano, Martín «El Tato» García. Aquí te dejamos el gol:

{{< tweet 1422758738797101063 >}}

Los «Caciques» apretaron hasta el final para emparejar el encuentro, pero no pudieron ante la solidez defensiva de los «verdolagas» en aguantar el resultado.

{{< tweet 1422936232716341254 >}}

##### Ficha técnica del partido


Diriangén| 0 | 1 | Marathon

Goles: Mario Martínez (48)

Alineación de Marathón: Denovan Torres, Luis Vega, Allans Vargas, Matías Techera, Isac Castillo, Emilio Izaguirre, Luis Garrido (Walter Ramírez) (32), Mario Martínez (Wilmer Crisanto) (81), Cristian Cálix (Ovidio Lanza) (69), Frelys López y Bayron Rodríguez (Carlo Costly) (69).

Siguiente reto para los verdolagas en la Liga Concacaf
El «Monstruo Verde» jugará el partido de vuelta en casa, en el Estadio Olímpico Metropolitano de San Pedro Sula, el próximo 18 de agosto, en búsqueda de sellar su pase hacia los octavos de final del torneo.

{{< tweet 1422779345836277760 >}}
